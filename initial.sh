#!/bin/bash

while [ -n "$1" ]; do
        echo "Working for $1"
	ssh root@"$1" 'apt-get update'
	ssh root@"$1" 'apt-get -y install isc-dhcp-client'
	ssh root@"$1" 'modprobe ath5k'
	ssh root@"$1" 'ifconfig wlan0 up'
        ssh root@"$1" 'hw_mode=b'
        ssh root@"$1" '/etc/init.d/hostapd restart'
	ssh root@"$1" 'iwconfig wlan0 channel 6'
	ssh root@"$1" 'iwlist wlan0 scan'
	ssh root@"$1" 'wpa_passphrase node1-1 SECRETPASSWORD > wpa.conf'
	ssh root@"$1" 'wpa_supplicant -iwlan0 -cwpa.conf -B'

	ssh root@"$1" 'dhclient wlan0'
	ssh root@"$1" 'ifconfig wlan0'
        ssh root@"$1" 'apt-get install iperf'	
	shift
done
