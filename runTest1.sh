#!/bin/bash
LOWRATE="$1"
HIGHRATE="11M"
P=0
ssh -o "StrictHostKeyChecking no" root@"$2" "pkill iperf"
ssh -o "StrictHostKeyChecking no" root@"$2" "nohup iperf -s > iperf.out 2> iperf.err < /dev/null &";
STR="$3"
while [-n "$4" ]; do
#echo "$4"
if [ $P -eq "0" ]; then
	ssh -o "StrictHostKeyChecking no" root@"$4" "mkdir "$STR"";
	ssh -o "StrictHostKeyChecking no" root@"$4" "iwconfig wlan0 rate "$LOWRATE"";
	ssh -o "StrictHostKeyChecking no" root@"$4" "sh -c 'cd ~/"$STR"/;nohup iperf -c 192.168.12.1 -u -b "$LOWRATE" -t 120  > iperf"$4".out 2> iperf.err < /dev/null &'";
fi

if [ $P -gt "0" ]; then
	ssh -o "StrictHostKeyChecking no" root@"$4" "mkdir "$STR"; cd "$STR"";
	ssh -o "StrictHostKeyChecking no" root@"$4" "iwconfig wlan0 rate 11M";
	ssh -o "StrictHostKeyChecking no" root@"$4" "sh -c 'cd ~/"$STR"/; nohup iperf -c 192.168.12.1 -u -b 11M -t 120  > iperf"$4".out 2> iperf.err < /dev/null &'";
fi
P=$((P+1))
shift

done

