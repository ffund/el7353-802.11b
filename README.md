# Performance anomaly of 802.11b #

In this work I am studying the performance anomaly of the 802.11 WLAN network when users with low link rate is present along with users with high link rates. The original work is presented in 

Heusse, Martin, et al. "Performance anomaly of 802.11b." INFOCOM 2003. Twenty-Second Annual Joint Conference of the IEEE Computer and Communications Societies. Vol. 2. IEEE, 2003.

The experiment is performed on a GENI wireless testbed ([ORBIT](https://geni.orbit-lab.org) or [WITest](https://witestlab.poly.edu)). You will need to reserve time on the testbed to run this experiment. The total runtime should be about 3 hours, but if you are not experienced with GENI wireless testbeds, you may want to reserve more time to familiarize yourself with the facility.

## Background ##

In this experiment we will see how channel contention and the presence of hosts with channel impairments (low link rate) adversely affects the performance of the entire 802.11b network. In fact there are two take away points from this experiment

* The throughput of each user decreases when the number of hosts in the network increases due to contention for the shared channel.

* When one host has a poor physical layer data rate it will occupy the channel longer, in turn degrading the application layer throughput of other users, even those with a good physical layer data rate.

This experiment will give us an insight into the performance of wireless networks with shared channels.

## Results ##

The following image is from the afore mentioned paper. It shows the useful data throughput obtained by any user, versus the number of nodes in the network, for four different scenarios: when all users have a physical layer data rate of 11 Mbps, when one slow user has 5.5 Mbps, when the slow user has 2 Mbps, and when the slow user has 1 Mbps.

![1208921-fig-3-large.gif](https://bitbucket.org/repo/LBKe9r/images/4212215149-1208921-fig-3-large.gif) 

The next image shows our reproduction of the work presented by Heusse, Martin, et al. The throughput experienced by one user is plotted against the number of users in the system, for the same four scenarios. Our results are similar to those in the original paper:

![fig1_1.jpg](https://bitbucket.org/repo/LBKe9r/images/2982952279-fig1_1.jpg) 

(Due to time and resource constraints, we do not increase the number of nodes all the way up to 20, as in the original paper.)

## Reproducing the experiment ##

In this section I will delineate the steps to reproduce the above results. For the sake of simplicity we will assume that there are 5 hosts and one access point (AP) in the network. The experiment can be very easily extrapolated to 15 or more hosts. We will use the grid on the Orbit test bed.

We require to create a set up with one AP which supports the 802.11b WLAN standard and connect N hosts to it (with increasing N). At the AP we run the Iperf UDP server, and from  N hosts (Iperf clients) we send UDP packets to the server. 

At each of the clients we will measure the **throughput** in Mb/s.

The measurements are take over a period of 120 sec. To reduce the variation, you are encouraged to use longer durations if time permits.

### Selecting a testbed ###

You may run this experiment on any of four different wireless testbeds. You will need to have reserved time on advance on one of these:

* The [WITest](https://witestlab.poly.edu) facility. 
* The "outdoor" facility on [ORBIT](https://geni.orbit-lab.org).
* The "sb4" facility on [ORBIT](https://geni.orbit-lab.org), with some modifications described in the "Notes" section at the end.
* The "grid" facility on [ORBIT](https://geni.orbit-lab.org). This has many WiFi nodes, but is heavily used and harder to schedule time on.

If you are using WITest, outdoor, or sb4, you can run this experiment with up to 8 hosts in the network. If you are using grid, you can use up to 20 hosts, as in the original experiment.

### Initial setup ###

At your reserved time, SSH into the testbed console (either `witestlab.poly.edu`, `outdoor.orbit-lab.org`, `sb4.orbit-lab.org`, or `grid.orbit-lab.org`) with your GENI wireless username and keys.

If you are using sb4, first follow the procedure described in the "Using sb4" section at the end of this page before proceeding.

Next, depending on what testbed you are using, define a environment variable called "GROUP" as follows:

* On **WITest**:

```
GROUP=omf.witest.node16,omf.witest.node17,omf.witest.node18,omf.witest.node19,omf.witest.node20,omf.witest.node21,omf.witest.node22,omf.witest.node23,omf.witest.node25
```

* On **outdoor**: 

```
GROUP=node1-1.outdoor.orbit-lab.org,node1-2.outdoor.orbit-lab.org,node1-3.outdoor.orbit-lab.org,node1-4.outdoor.orbit-lab.org,node1-5.outdoor.orbit-lab.org,node1-8.outdoor.orbit-lab.org,node1-9.outdoor.orbit-lab.org,node2-2.outdoor.orbit-lab.org,node2-7.outdoor.orbit-lab.org,node2-8.outdoor.orbit-lab.org
```

* On **sb4**: 

```
GROUP=node1-1.sb4.orbit-lab.org,node1-2.sb4.orbit-lab.org,node1-3.sb4.orbit-lab.org,node1-4.sb4.orbit-lab.org,node1-5.sb4.orbit-lab.org,node1-6.sb4.orbit-lab.org,node1-7.sb4.orbit-lab.org,node1-8.sb4.orbit-lab.org,node1-9.sb4.orbit-lab.org`
```

If some of these nodes are unavailable, you may have to make some substitutions. You can substitute any node equipped with an ath5k- or ath9k-compatible WiFi card.

Then, after you have run the appropriate command to set the `GROUP` variable, load the disk image onto all nodes in the group:

    omf-5.4 load -i wifi-experiment.ndz -t "$GROUP"

and turn on all the nodes once imaging is done

    omf tell -a on -t "$GROUP"

Next, open another two terminal sessions and SSH into the testbed console in each. You should now have three terminal windows:

* One on which you will configure and run commands on the AP,
* One on which you will configure and run commands on all of the "fast" hosts in the network, and
* One on which you will configure and run commands on the "slow" host.

### Create an AP ###

Select one node in your topology to be the AP. This should be a node with an ath9k-compatible wireless card, and it should be near the middle of the group of nodes. (We recommend: node20 on WITest, node1-5 on outdoor or sb4.) Use SSH to log in to the node as the "root" user.

Once logged on to the node, open a new configuration file

```
nano hostapd.conf
```

and paste the following contents inside:

```
beacon_int=100
ssid=anomaly
interface=ap0
driver=nl80211
channel=1
hw_mode=b
ieee80211n=0
```

Then save the file (Ctrl+O and Enter) and quit the nano editor (Ctrl+X).

Finally, bring up the AP with 

```
iw dev wlan0 interface add ap0 type __ap
ifconfig ap0 192.168.0.1 netmask 255.255.0.0 up
hostapd -B hostapd.conf
```

This will bring up an 802.11b wireless network named "anomaly" on channel 1, and the access point will have the address 192.168.0.1.

### Configure 802.11b hosts ###

To demonstrate this experiment, we will start with the scenario where there is one "fast" and one "slow" host. Designate one node in your topology as the "fast" host and one as the "slow" host. Then, in your remaining two terminals, SSH into the fast and slow host, respectively, as the root user.

Then, configure both hosts to connect to the 802.11b network, by running the following commands on both hosts.

First, load the wireless interface drivers in case they aren't already loaded:

```
modprobe ath9k
modprobe ath5k
```

Then connect to the "anomaly" network:

```
ifconfig wlan0 up
iwconfig wlan0 essid anomaly
```

Verify that you are connected by running

```
iwconfig wlan0
```

The output should look something like this:

```
wlan0     IEEE 802.11abgn  ESSID:"anomaly"  
          Mode:Managed  Frequency:2.412 GHz  Access Point: 00:15:6D:84:FB:63   
          Bit Rate=1 Mb/s   Tx-Power=15 dBm   
          Retry  long limit:7   RTS thr:off   Fragment thr:off
          Encryption key:off
          Power Management:off
          Link Quality=51/70  Signal level=-59 dBm  
          Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0
          Tx excessive retries:0  Invalid misc:7   Missed beacon:0
```

Finally, set an IP address as

```
IPSUFFIX=$(ip route get 8.8.8.8 | awk '{print $NF;exit}' | cut -d'.' -f 3-4)
ifconfig wlan0 192.168."$IPSUFFIX" netmask 255.255.0.0
```

(This will assign a unique IP address to each wireless host in the network, based on the node number.)

Verify that each host can reach the AP:

```
ping -c 5 192.168.0.1
```

### Run the test ###

Running the test to measure the throughput is simple.

First, run  the iperf server on the AP in UDP mode.

```
iperf -s -u | tee iperf-ap.out
```

Try to run the next series of steps quickly, so that each iperf starts sending data at about the same time.

On the node that is designated as having a "fast" link rate, set the link rate to 11 Mbps and run iperf in the client mode:

```
iwconfig wlan0 rate 11M
iperf -c 192.168.0.1 -u -b 11M -t 120 | tee iperf-$(hostname).out
```

Finally, on the node that is designated as the "slow" host, set the link rate to a lower permissible value (1,2 or 5.5 Mbps) and run iperf in client mode. For example, to test the scenario with a 2Mbps "slow" host:

```
iwconfig wlan0 rate 2M
iperf -c 192.168.0.1 -u -b 2M -t 120  | tee iperf-$(hostname).out
```

The experiment will take 120 seconds. The iperf output will be redirected to a file, in addition to being displayed in the terminal, so that after it is finished you can analyze the data.


### Scaling up ###

These steps might be daunting to execute on a large number of machines. We can make it easier to scale up the experiment by using [tmux](http://manpages.ubuntu.com/manpages/xenial/man1/tmux.1.html) to send commands to multiple terminals at once. This way, we can send a command (or series of commands) to a large number of "fast" hosts at once.

First, create a shell variable called "HOSTS" in which you list all of the "fast" hosts in your experiment. For example, on WITest, if you are using node20 and the AP and node21 as the "slow" node,

```
HOSTS="node16 node17 node18 node19 node22 node23 node24 node25"
```
Then, run

```
tmux new-session -d -s anomaly "ssh root@$HOSTS[0]"
for i in ${HOSTS[@]}
do
    tmux split-window -v -t anomaly "ssh root@$i"
    tmux select-layout tiled
done
tmux set-window-option synchronize-panes on
tmux attach -t anomaly
```

This will open an SSH session to each host listed in the "HOSTS" variable. When you type in this terminal, commands will be broadcast to _all_ hosts in the list.


### Collect Data ###

Timing is crucial in shared resources. As soon as you are sone with the experiments collect your data from the nodes using **scp**. All your data will be lost once the nodes are reset, i.e you lose access to the grid. The following commands are needed

    mkdir exp2
    scp root@node1-2:~/exp2/* ~/exp2/

This will bring all the files from the **exp2** folder in node1-2 to the **exp2/** folder on your Orbit terminal. The 8th line of the Iperf data file contains the summary of the experiment. Read that on a file as

    sed '8q;d' iperfnode1-2.out >> exp2-1.txt

Do this for all the files gathered from all the nodes for a particular experiment. Obviously this file is not suitable to be used by data processing tools like MATLAB, Excel etc. Extract the throughput from this file as

    cat exp2-1.txt | awk '{IF($8=="Kbits/sec") print $7/1000; else print $7}'

which writes all the throughput data in Mbps. A MATLAB script with the data files obtained can be found within the **data.zip** file.

The number of data files generated using Iperf is large in this experiment and naming may vary from user to user. Using the above two commands and a loop in the shell script one can very easily extract the data.

## Additional Notes ##

* Be sure you can ssh to all the hosts before executing any of the scripts.
* In this work the experiment run time is 120 sec. This is a limiting factor for large number of hosts. But running longer experiments is not practical on shared resources.


### Using sb4 ###

If you are using the sb4 facility, when you first log in to the sb4 console, you should reset its [programmable attenuation matrix](http://www.orbit-lab.org/wiki/Hardware/bDomains/cSandboxes/dSB4) to zero attenuation between all pairs of nodes. From the "sb4" console, run

```
wget -qO- "http://internal2dmz.orbit-lab.org:5054/instr/setAll?att=0"


wget -qO- "http://internal2dmz.orbit-lab.org:5054/instr/selDevice?switch=1&port=1"  
wget -qO- "http://internal2dmz.orbit-lab.org:5054/instr/selDevice?switch=2&port=1"  
wget -qO- "http://internal2dmz.orbit-lab.org:5054/instr/selDevice?switch=3&port=1"  
wget -qO- "http://internal2dmz.orbit-lab.org:5054/instr/selDevice?switch=4&port=1"  
```