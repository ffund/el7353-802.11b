#!/bin/bash

FILE="$1"
DIR="$2"
cd "$2"

while [ -n "$3" ]; do
	sed '8q;d' iperf"$3".out >> "$FILE"
shift
done

cd ..
